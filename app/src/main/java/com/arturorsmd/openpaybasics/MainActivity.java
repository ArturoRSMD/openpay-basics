package com.arturorsmd.openpaybasics;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import mx.openpay.android.Openpay;
import mx.openpay.android.OperationCallBack;
import mx.openpay.android.OperationResult;
import mx.openpay.android.exceptions.OpenpayServiceException;
import mx.openpay.android.exceptions.ServiceUnavailableException;
import mx.openpay.android.model.Card;
import mx.openpay.android.model.Token;
import mx.openpay.android.validation.CardValidator;

public class MainActivity extends AppCompatActivity {

    Openpay openpay;
    String merchantId = "your_merchantID";
    String apiKey = "your_api_key";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initOpenPay();

        if (itIsAValidCard("Test", "4242424242424242", 11, 21, "111")){
            Card card = generateCard("Test", "4242424242424242", 11, 21, "222");
            createToken(card);
        }





    }

    public void initOpenPay(){
        openpay  = new Openpay(merchantId, apiKey, false);
    }

    public Card generateCard(String holderName, String cardNumber, int expirationMonth, int expirationYear, String cvv2){

        Card card = new Card();
        card.holderName(holderName);
        card.cardNumber(cardNumber);
        card.expirationMonth(expirationMonth);
        card.expirationYear(expirationYear);
        card.cvv2(cvv2);
        return card;
    }

    public void createToken(Card card){
        openpay.createToken(card, new OperationCallBack<Token>() {
            @Override
            public void onError(OpenpayServiceException error) {

                switch( error.getErrorCode()){
                    case 3001:
                        Toast.makeText(MainActivity.this, "Declined", Toast.LENGTH_SHORT).show();
                        break;
                    case 3002:
                        Toast.makeText(MainActivity.this, "Expired", Toast.LENGTH_SHORT).show();
                        break;
                    case 3003:
                        Toast.makeText(MainActivity.this, "Insufficient Funds", Toast.LENGTH_SHORT).show();
                        break;
                    case 3004:
                        Toast.makeText(MainActivity.this, "Stolen Card", Toast.LENGTH_SHORT).show();
                        break;
                    case 3005:
                        Toast.makeText(MainActivity.this, "Suspected Fraud", Toast.LENGTH_SHORT).show();
                        break;

                    case 2002:
                        Toast.makeText(MainActivity.this, "Already Exists", Toast.LENGTH_SHORT).show();
                        break;
                    default:
                        Toast.makeText(MainActivity.this, "Error Creating Card", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onCommunicationError(ServiceUnavailableException e) {

                Toast.makeText(MainActivity.this, "Error de comunicacion", Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onSuccess(OperationResult<Token> operationResult) {

                Toast.makeText(MainActivity.this, "Operacion exitosa", Toast.LENGTH_SHORT).show();

            }
        });
    }

    public boolean itIsAValidCard(String holderName, String cardNumber, int expirationMonth, int expirationYear, String cvv2){
        if (!CardValidator.validateCard(holderName, cardNumber, expirationMonth, expirationYear, cvv2)){
            Toast.makeText(this, "Tarjeta error", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }





}
